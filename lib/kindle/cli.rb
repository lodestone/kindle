require 'thor'
require 'highline/import'
require 'io/console'

module Kindle
  class CLI < Thor
    desc "highlights", "Display Kindle Highlights"
    def highlights
      puts ">> Kindle Highlights Fetcher::::::::::::::::::::::::::::::::::::::::"
      kindle = Kindle::Account.new
      login  = kindle.settings.username
      passwd = kindle.settings.password

      if login
        puts "Using Amazon username: #{login}"
      else
        ask("Enter your Amazon username:  ") { |q| q.echo = true }
      end

      if passwd
        puts "Using stored Amazon password: ••••••••••••"
      else
        print "Enter your Amazon password (This is *not* stored): "
        passwd = STDIN.noecho(&:gets).chomp
        print "\n"
      end

      begin
        kindle.login = login
        kindle.password = passwd
        puts "Getting your kindle highlights..."
        highlights = kindle.highlights
        highlights.each do |highlight|
          puts "#{highlight.asin};#{highlight.title};#{highlight.author};#{highlight.highlight}"
        end
      rescue => ex
        # TODO Actually handle this!
        puts ex
        puts "Crud, something went wrong..."
      end
    end

    # Usage:
    # TODO: Pass in something to bide our time
    # TODO: Multiple output formats. CSV, JSON, Pretty, HTML?
    # TODO: Cache results!
    # > kindle help
    # > kindle highlights -t json
    # > kindle highlights --type=json
    # > kindle documents -t pretty --only="/Book Title Regex/"
    # > kindle highlights -t json
    # > kindle highlights -t json
  end
end
